<?php

namespace App\Controller;

use LetterInput;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Роут возвращает "разрешённые" буквы.
 */
class GetLetters
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(
        ValidatorInterface $validator
    ) {
        $this->validator = $validator;
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function __invoke(Request $request): JsonResponse
    {
        $input = new LetterInput($request->query->get('input'));

        $errors = $this->validator->validate($input);

        if (count($errors) > 0) {
            return new JsonResponse(
                [
                    'errorMassage' => 'Invalid string'
                ],
                Response::HTTP_BAD_REQUEST
            );
        }

        return new JsonResponse($input->getInput(), Response::HTTP_OK);

    }
}