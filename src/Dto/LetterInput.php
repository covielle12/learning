<?php

use App\Validator as MyConstrains;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * DTO входящих данных.
 */
class LetterInput
{
    /**
     * @MyConstrains\ContainsAllowedLetters
     * @Assert\NotBlank
     *
     * @var string
     */
    private $input;

    public function __construct($input)
    {
        $this->input = $input;
    }

    public function getInput(): string
    {
        return $this->input;
    }
}