<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ContainsAllowedLetters extends Constraint
{
    const ALLOWED_LETTERS = [
        'a',
        'b',
        'c'
    ];

    public $message = 'Invalid input.';
}
